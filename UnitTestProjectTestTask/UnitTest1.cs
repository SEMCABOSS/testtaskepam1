﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FigureLibrary;

namespace UnitTestProjectTestTask
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// TestMethodCreateFigures.
        /// </summary>
        [TestMethod]
        public void TestMethodCreateFigures()
        {
            FigureHelper figure = new FigureHelper(@"D:\test.txt");

            Assert.AreEqual(3, figure.Figures.Count);
        }

        /// <summary>
        /// TestMethodCalculatAveragePerimeterAllFigures.
        /// </summary>
        [TestMethod]
        public void TestMethodCalculatAveragePerimeterAllFigures()
        {
            FigureHelper figure = new FigureHelper(@"D:\test.txt");

            Assert.AreEqual(12, (int)figure.CalculatAveragePerimeterAllFigures());
        }

        /// <summary>
        /// TestMethodCalculatAverageAreaAllFigures.
        /// </summary>
        [TestMethod]
        public void TestMethodCalculatAverageAreaAllFigures()
        {
            FigureHelper figure = new FigureHelper(@"D:\test.txt");

            Assert.AreEqual(42, (int)figure.CalculatAverageAreaAllFigures());
        }

        /// <summary>
        /// TestMethodFindFigureTypeWithLargestAveragePerimeter.
        /// </summary>
        [TestMethod]
        public void TestMethodFindFigureTypeWithLargestAveragePerimeter()
        {
            FigureHelper figure = new FigureHelper(@"D:\test.txt");

            Assert.AreEqual("Circle", figure.FindFigureTypeWithLargestAveragePerimeter());
        }

        /// <summary>
        /// TestMethodFindFigureWithLargestArea.
        /// </summary>
        [TestMethod]
        public void TestMethodFindFigureWithLargestArea()
        {
            FigureHelper figure = new FigureHelper(@"D:\test.txt");

            Assert.AreEqual("Circle", figure.FindFigureWithLargestArea().GetType().Name);
        }
    }
}
