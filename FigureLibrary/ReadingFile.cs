﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FigureLibrary
{
    static class ReadingFile
    {

        /// <summary>
        /// Чтение файла.
        /// </summary>
        /// <returns>Массив данных.</returns>
        public static String[] ReadFile(string patch)
        {
            String[] lines = File.ReadAllLines(patch);
            return lines;
        }
    }
}
