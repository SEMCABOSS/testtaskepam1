﻿using System;
using System.Collections.Generic;

namespace FigureLibrary
{
    public abstract class Figure
    {
        /// <summary>
        /// Area.
        /// </summary>
        public virtual double Area { get; set; }

        /// <summary>
        /// Perimeter.
        /// </summary>
        public virtual double Perimeter { get; set; }

        /// <summary>
        /// Sides.
        /// </summary>
        public virtual List<Int32> Sides { get; set; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="param">Стороны фигруы.</param>
        public Figure(String[] param)
        {
            Sides = new List<int>();

            for (int i = 1; i < param.Length; i++)
            {
                Sides.Add(Int32.Parse(param[i]));
            }

            Area = CalculatArea();
            Perimeter = CalculatePerimeter();
        }

        /// <summary>
        /// CalculatArea.
        /// </summary>
        /// <returns>Area.</returns>
        public abstract double CalculatArea();

        /// <summary>
        /// CalculatePerimeter.
        /// </summary>
        /// <returns>Perimeter.</returns>
        public abstract double CalculatePerimeter();
    }
}
