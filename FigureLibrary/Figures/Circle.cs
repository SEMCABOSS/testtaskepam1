﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureLibrary
{
    public class Circle : Figure
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="param">Стороны фигруы.</param>
        public Circle(String[] param)
        : base(param)
        {

        }

        public override double CalculatArea()
        {
            return Math.PI * Sides[0] * Sides[0];
        }

        public override double CalculatePerimeter()
        {
            return 2 * Math.PI * Sides[0];
        }
    }
}
