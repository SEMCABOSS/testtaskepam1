﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureLibrary
{
    public class Rectangle : Figure
    {

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="param">Стороны фигруы.</param>
        public Rectangle(String[] param)
        : base(param)
        {

        }

        public override double CalculatArea()
        {
            return Sides[0] * Sides[1];
        }

        public override double CalculatePerimeter()
        {
            return 2 * (Sides[0] + Sides[1]);
        }
    }
}
