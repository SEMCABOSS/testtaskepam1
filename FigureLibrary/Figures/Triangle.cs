﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureLibrary
{
    public class Triangle : Figure
    {

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="param">Стороны фигруы.</param>
        public Triangle(String[] param)
        : base(param)
        {

        }


        public override double CalculatArea()
        {
            double perimeter = CalculatePerimeter() / 2 ;

            return Math.Sqrt(perimeter * (perimeter - Sides[0]) * (perimeter - Sides[1]) * (perimeter * Sides[2]));
        }

        public override double CalculatePerimeter()
        {
            return Sides[0] + Sides[1] + Sides[2];
        }
    }
}
