﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;

namespace FigureLibrary
{
    public class FigureHelper
    {
        /// <summary>
        /// Figures.
        /// </summary>
        public List<Figure> Figures { get; set; }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="patch">patch</param>
        public FigureHelper(string patch)
        {
            Figures = CreateFigures(ReadingFile.ReadFile(patch));
        }

        /// <summary>
        /// CreateFigures.
        /// </summary>
        /// <param name="figureData">Массив данных фигур.</param>
        /// <returns>Массив фигур.</returns>
        public static List<Figure> CreateFigures(string[] figureData)
        {
            CreateFigure creator = null;
            List<Figure> figures = new List<Figure>();

            string checkFigure;


            for (int i = 0; i < figureData.Length; i++)
            {
                string[] param;

                param = figureData[i].Split(' ');

                checkFigure = param[0];

                switch (checkFigure)
                {
                    case "Triangle":
                        creator = new CreateTriangle();
                        break;

                    case "Rectangle":
                        creator = new CreateRectangle();
                        break;

                    case "Circle":
                        creator = new CreateCircle();
                        break;

                }

                if (creator != null)
                {

                    figures.Add(creator.Create(param));
                }
            }

            return figures;
        }

        /// <summary>
        /// CalculatAverageAreaAllFigures.
        /// </summary>
        /// <returns>double allArea.</returns>
        public double CalculatAverageAreaAllFigures()
        {
            double allArea = 0;

            for (int i = 0; i < Figures.Count; i++)
            {
                allArea += Figures[i].CalculatArea();
            }

            allArea = allArea / Figures.Count;

            return allArea;
        }

        /// <summary>
        /// CalculatAveragePerimeterAllFigures.
        /// </summary>
        /// <returns>double allPerimeter.</returns>
        public double CalculatAveragePerimeterAllFigures()
        {
            double allPerimeter = 0;

            for (int i = 0; i < Figures.Count; i++)
            {
                allPerimeter += Figures[i].CalculatePerimeter();
            }

            allPerimeter = allPerimeter / Figures.Count;

            return allPerimeter;
        }

        /// <summary>
        /// FindFigureTypeWithLargestAveragePerimeter.
        /// </summary>
        /// <returns>string typeFigure.</returns>
        public string FindFigureTypeWithLargestAveragePerimeter()
        {
            List<Figure> figures = new List<Figure>();
            var result =  Figures.OrderByDescending(figure => figure.Perimeter / figure.Sides.Count);

            foreach(Figure figure in result)
            {
                figures.Add(figure);
            }

            return figures[0].GetType().Name;
        }

        /// <summary>
        /// FindFigureWithLargestArea.
        /// </summary>
        /// <returns>Figure.</returns>
        public Figure FindFigureWithLargestArea()
        { 
            List<Figure> figures = new List<Figure>();
            var result = Figures.OrderByDescending(figure => figure.Area);

            foreach (Figure figure in result)
            {
                figures.Add(figure);
            }

            return figures[0];
        }
    }
}
