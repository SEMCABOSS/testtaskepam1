﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureLibrary
{
    class CreateTriangle : CreateFigure
    {
        /// <summary>
        /// Create.
        /// </summary>
        /// <param name="param">Данные.</param>
        /// <returns>new Triangle.</returns>
        public override Figure Create(String[] param)
        {
            return new Triangle(param);
        }
    }
}
