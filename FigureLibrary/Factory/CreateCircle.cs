﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureLibrary
{
    class CreateCircle : CreateFigure
    {
        /// <summary>
        /// Create.
        /// </summary>
        /// <param name="param">Данные.</param>
        /// <returns>new Circle.</returns>
        public override Figure Create(String[] param)
        {
            return new Circle(param);
        }
    }
}
