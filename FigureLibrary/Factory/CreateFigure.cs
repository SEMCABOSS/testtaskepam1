﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureLibrary
{
    abstract class CreateFigure
    {
        /// <summary>
        /// Фабричный метод.
        /// </summary>
        /// <param name="param">Стороны.</param>
        /// <returns>Figures.</returns>
        abstract public Figure Create(String[] param);
    }
}
