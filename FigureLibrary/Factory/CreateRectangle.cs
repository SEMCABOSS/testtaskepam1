﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureLibrary
{
    class CreateRectangle : CreateFigure
    {
        /// <summary>
        /// Create.
        /// </summary>
        /// <param name="param">Данные.</param>
        /// <returns>new Rectangle.</returns>
        public override Figure Create(String[] param)
        {
            return new Rectangle(param);
        }
    }
}
